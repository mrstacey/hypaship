jQuery.noConflict();
(function( $ ) {
	
	$(function() {
		
		function knockoutArrow(){
			var activeIcon = $('.testimonial-logos').find('.active img');
			var parentWidth = $(window).width();
			var activeWidth = activeIcon.width();
			var leftToCenter = activeIcon.offset().left + (activeIcon.width()/2);
			var distanceInPercent = (leftToCenter/parentWidth) * 100 ;
			var arrowLeft = $('.testimonials').find('span.left');
			var arrowRight = $('.testimonials').find('span.right');
			
			arrowLeft.css({'width': distanceInPercent-0.5+'%'});
			arrowRight.css({'width': (100-distanceInPercent)-0.5+'%' });
		}
		
		function buildClientSlider(){
			var wrapper = $('.testimonial-holder');
			var slides = $('.testimonial-holder').find('.testimonial');
			var activeIcon = $('.testimonial-logos').find('.active');
			wrapper.css('width', slides.length*100+'%');
		}
		
		function transitionHero(){
			var wrapper = $('.slide-wrapper');
			wrapper.find('.slides').last().fadeTo(1000, 0, function(){
				$(this).prependTo($(this).parent()).css('opacity',1);
			});
		}
		function historySlider(visible){
			if(!visible) {
				var visible = 3.5;
			}
			var wrapper = $('.history-wrapper');
			var wrapperWidth = wrapper.width();	
			var holder = $('.history-holder');
			var milestones = holder.find('.milestone');
			var countMilestones = milestones.length;
			var milestoneWidth = wrapperWidth/visible;

			var completedStep = 0;
			var completedHalfStep = false;
			var wholeSteps = Math.floor(countMilestones - visible);
			var halfStepNeed = visible % 1 === 0 ? false : true;

			var halfStep = parseInt(milestoneWidth)/2;
			
			holder.css('width', milestoneWidth * countMilestones);
			holder.find('hr').css('width', milestoneWidth * (countMilestones-1));
			$.each(milestones, function(){
				$(this).css('width', milestoneWidth +'px');
			});
			
			$('.move-history.forward').click(function(){
				
				if(!$(this).hasClass('disabled')){
					
					$('.move-history.back').removeClass('disabled');
					
					if(completedStep == wholeSteps && halfStepNeed == true) {
						holder.css('margin-left', (parseInt(milestoneWidth*completedStep)+parseInt(halfStep))*-1+'px');
						completedStep = 0;
						completedHalfStep = true;
						$(this).addClass('disabled');
					}else{
						holder.css('margin-left',parseInt((milestoneWidth*(completedStep+1)))*-1+'px');
						completedStep++;
						if(completedStep == wholeSteps && halfStepNeed == false){
							$(this).addClass('disabled');
						}
					}
				}
				
				return false;
				
			});
			
			$('.move-history.back').click(function(){
				
				if(!$(this).hasClass('disabled')){
					
					$('.move-history.forward').removeClass('disabled');
					
					if(completedHalfStep == true) {
						holder.css('margin-left', (parseInt(holder.css('margin-left'))+parseInt(halfStep))+'px');
						completedHalfStep = false;
					}else{
						if(completedStep-1 < 0) {
							holder.css('margin-left', 0);
						}else{
							holder.css('margin-left',parseInt(milestoneWidth*Math.abs(completedStep-1))*-1+'px');
						}
						completedStep--;
						if(completedStep <= 0){
							$(this).addClass('disabled');
						}
					}
				}
				
				return false
			});
		}
	
		$( document ).ready(function() {
					
			if($('.slide-wrapper').length){
				var loop = setInterval(function(){
					transitionHero();
				}, 5000);
			}
			
			
			if($('.testimonial-holder').length){
				buildClientSlider();
				knockoutArrow();
				
				var loop = setInterval(function(){
					var activeIcon = $('.testimonial-logos').find('.active');
					var newIcon = activeIcon.index()+1 > $('.testimonial-logos div.col').length-1 ? 0 : activeIcon.index()+1;
					
					$('.testimonial-holder').animate({'left':'-'+(newIcon)*100+'%'}, 'slow', 'swing');
					activeIcon.removeClass('active');
					
					$('.testimonial-logos div.col').eq(newIcon).addClass('active');
					
					knockoutArrow();
				
				}, 5000);
			}
			
			if($(window).width() > 960){
				historySlider(3.5);
			}else{
				historySlider(1);
			}
			
			
			
			$('.show-more').click(function(){
				var clicked = $(this);
				var target = $(this).data('target');
				var closestTarget = clicked.prev(target);
				console.log(closestTarget);

				if(closestTarget.hasClass('d-none')){
					closestTarget.removeClass('d-none').animate({opacity:1});
					clicked.text('- Show less');
				}else{
					closestTarget.animate({opacity:0},500,function(){
						closestTarget.addClass('d-none');
						clicked.text('- Show more');
					});
				}
				
				return false;
			})
	
		});
	
	});
})(jQuery);