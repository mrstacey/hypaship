<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery');
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

function register_menus() {
	register_nav_menu('footer-1',__( 'Footer 1' ));
	register_nav_menu('footer-2',__( 'Footer 2' ));
	register_nav_menu('footer-3',__( 'Footer 3' ));
}
add_action( 'init', 'register_menus' );

function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function HTMLToRGB($htmlCode){
	if($htmlCode[0] == '#')
	  $htmlCode = substr($htmlCode, 1);
	
	if (strlen($htmlCode) == 3){
	  $htmlCode = $htmlCode[0] . $htmlCode[0] . $htmlCode[1] . $htmlCode[1] . $htmlCode[2] . $htmlCode[2];
	}
	
	$r = hexdec($htmlCode[0] . $htmlCode[1]);
	$g = hexdec($htmlCode[2] . $htmlCode[3]);
	$b = hexdec($htmlCode[4] . $htmlCode[5]);
	
	return $b + ($g << 0x8) + ($r << 0x10);
}

function RGBToHSL($RGB) {
	$r = 0xFF & ($RGB >> 0x10);
	$g = 0xFF & ($RGB >> 0x8);
	$b = 0xFF & $RGB;
	
	$r = ((float)$r) / 255.0;
	$g = ((float)$g) / 255.0;
	$b = ((float)$b) / 255.0;
	
	$maxC = max($r, $g, $b);
	$minC = min($r, $g, $b);
	
	$l = ($maxC + $minC) / 2.0;
	
	if($maxC == $minC){
	  $s = 0;
	  $h = 0;
	}else{
	  if($l < .5){
	    $s = ($maxC - $minC) / ($maxC + $minC);
	  }else{
	    $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
	  }
	  if($r == $maxC)
	    $h = ($g - $b) / ($maxC - $minC);
	  if($g == $maxC)
	    $h = 2.0 + ($b - $r) / ($maxC - $minC);
	  if($b == $maxC)
	    $h = 4.0 + ($r - $g) / ($maxC - $minC);
	
	  $h = $h / 6.0; 
	}
	
	$h = (int)round(255.0 * $h);
	$s = (int)round(255.0 * $s);
	$l = (int)round(255.0 * $l);
	
	return (object) Array('hue' => $h, 'saturation' => $s, 'lightness' => $l);
}