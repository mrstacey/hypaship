<div class="jumbotron">
		<?php if(isset($bannerBg) && $bannerBg){
			$style = "background: url(".$bannerBg['url'].") no-repeat center center; background-size: cover";
			$opacity = "opacity: 0.7";
		}else{
			$style;
			$opacity = "opacity: 1";
		}
		?>
		<div class="slide d-flex align-items-stretch" style="<?php echo $style; ?>">
			
			<div class="h-100 w-100 position-absolute" style="background-color:<?php echo $primaryColor.'; '.$opacity;?>;"></div>
			
			<div class="container hero-content">
				
				<div class="row align-items-center h-100 justify-content-center">
					
					<?php if(isset($bannerScreenshot) && $bannerScreenshot){?>
					
						<div class="col-6 text-right screenshot">
							<img src="<?php echo $bannerScreenshot['url'];?>">
						</div>
						<div class="col-4 pt-5">
							<?php if($bannerLogo){?>
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
							<?php }else{ 
								the_title();
							}
							if($bannerContent){
								echo '<p class="lead">'.$bannerContent.'</p>';
							};
							if('products' == get_post_type() ) {?>
								<a href="#" class="btn btn-light">Enquire</a>
							<?php }?>
						</div>
						
					<?php }else{ ?>
					
						<div class="col-6 text-center">
							<?php if($bannerLogo){?>
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
							<?php }else{ 
								the_title();
							}
							if($bannerContent){
								echo '<p>'.$bannerContent.'</p>';
							};
							
							if('products' == get_post_type() ) {?>
								<a href="#" class="btn btn-light">Enquire</a>
							<?php }?>
						</div>
						
					<?php }?>
				</div>
			</div>
			
		</div>
		
	</div>