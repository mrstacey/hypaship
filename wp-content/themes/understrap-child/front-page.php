<?php
/**
 * The template for displaying front page.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">
	
	<div class="jumbotron">
		<?php 
		$heroArgs = array(
			'posts_per_page'   => 5,
			'orderby'          => 'title',
			'order'            => 'DESC',
			'post_type'        => 'hero',
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);
		$heroArray = get_posts( $heroArgs ); 
		
		$slidecount=0;?>
		
		<div class="slide-wrapper">
		
		<?php foreach ( $heroArray as $hero ) : $slidecount++;
			
			$gradientTop = get_field('gradient_overlay_top', $hero->ID);
			$gradientBottom = get_field('gradient_overlay_bottom', $hero->ID);
			
		?>
		
		<div class="slides d-flex align-items-stretch" style="background: url('<?php echo get_the_post_thumbnail_url($hero->ID) ;?>') no-repeat center center; background-size: cover;">
			
		<?php if( have_rows('content', $hero->ID) ): ?>
			
			<?php while ( have_rows('content', $hero->ID) ) : the_row();
		    	
		    	if( get_row_layout() == 'services' ): ?>

				<div class="slide slide-<?php echo $slidecount;?> bg-wrapper d-flex align-items-end" style="background: linear-gradient(180deg, <?php echo $gradientTop;?> 0%, <?php echo $gradientBottom;?> 100%)">
		
					<div class="container hero-content">
					
									    		
				    		<div class="row align-self-end">
					    		<div class="col-md-5 col">
									<?php 
						    		the_sub_field('title'); 
						    		$content = $hero->post_content;
									$content = apply_filters('the_content', $content);
									$content = str_replace(']]>', ']]&gt;', $content);
									echo $content; ?>
					    		</div>
				    		</div>
							
							<?php if( have_rows('logos', $hero->ID) ): ?>
								
							<div class="row">
								
								<div class="col-md-8 col mb-4 mt-3">
								
									<div class="row">
										
								<?php while ( have_rows('logos', $hero->ID) ) : the_row();
							
								$logos = get_sub_field('logo_images'); 
								
								if($logos){	
									echo '<div class="col"><img class="" src="'.$logos['url'].'"></div>';
									
								}
							
								endwhile; ?>
								
									</div>
							
								</div>
								
							</div>
							
							<?php endif; ?>
							
							
							
							<?php echo '<a class="btn btn-primary btn-lg" href="'.get_field('button_link', $hero->ID).'" role="button">'.get_field('button_text', $hero->ID).'</a>'; ?>
							
					</div>
					
				</div>
				
		
							
				<?php elseif (get_row_layout() == 'screen_shot'): ?>
				    	
				<div class="slide slide-<?php echo $slidecount;?> bg-wrapper d-flex position-absolute h-100" style="background: linear-gradient(180deg, <?php echo $gradientTop;?> 0%, <?php echo $gradientBottom;?> 100%)">
					
					<div class="container hero-content">
						
			    	<?php
			    		$bannerScreenshot = get_sub_field('screenshot', $hero->ID);
			    		$bannerLogo = get_sub_field('logo', $hero->ID);
			    		
			    		$bannerContent = $hero->post_content;
						$bannerContent = apply_filters('the_content', $bannerContent);
						$bannerContent = str_replace(']]>', ']]&gt;', $bannerContent);
					
			    	?>
						<div class="row align-items-md-center align-items-end h-100">
							
							<div class="col-6 order-12 order-md-1 text-right screenshot">
								<img src="<?php echo $bannerScreenshot['url'];?>">
							</div>
							
							<div class="col-md-4 col-6 order-1 order-md-12 pt-5 pb-md-5 text-center text-md-left">
								
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
								<?php if($bannerContent){
									echo $bannerContent;
								};?>
								<a href="<?php the_field('button_link', $hero->ID) ;?>" class="btn btn-primary">Find out more</a>
								
							</div>
						</div>
						
					</div>
					
				</div>
				
				
				
				<?php elseif (get_row_layout() == 'testimonial'): ?>
				    	
				<div class="slide slide-<?php echo $slidecount;?> bg-wrapper d-flex testimonial-slide" style="background: linear-gradient(180deg, <?php echo $gradientTop;?> 0%, <?php echo $gradientBottom;?> 100%)">
					
					<div class="container hero-content">
						
						<div class="row align-items-end align-items-md-center justify-content-center h-100">
							
							<div class="col-md-8 text-md-center text-left">
								<?php 
									
									$testContent = $hero->post_content;
									$testContent = apply_filters('the_content', $testContent);
									$testContent = str_replace(']]>', ']]&gt;', $testContent);
								
								echo $testContent;
								
								$icon = get_sub_field('client-logo', $hero->ID);?>
								<p class="text-md-center text-left"><img src="<?php echo $icon['url'];?>"></p>
								
								<a href="<?php the_field('button_link', $hero->ID) ;?>" class="btn btn-primary">View our clients</a>
								
							</div>
							
						</div>
						
					</div>
					
				</div>

				
				<?php endif;
						
				endwhile; ?>
										
			<?php endif; ?>
			
		</div>
		
		<?php endforeach;?>
		
		</div>
		
	</div>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<?php 
		$clientArgs = array(
			'posts_per_page'   => 5,
			'orderby'          => 'title',
			'order'            => 'DESC',
			'post_type'        => 'testimonial',
			'post_status'      => 'publish',
			'suppress_filters' => true,
		);
		$clientArray = get_posts( $clientArgs ); 
		
		?>
	
		
		<div class="row pt-3 pb-2">
			
			<div class="col-md-6 d-flex align-items-center">
				
				<h4 class="mb-2 mb-md-0">Some of our Clients</h4>
				
			</div>
			
			<div class="col-md-6 d-flex align-items-center testimonial-logos">
				<div class="row">
					<?php 
					$count = 0;
					foreach ( $clientArray as $clientIcon ) : $count++;?>
						<?php $icon = get_field('client-icon', $clientIcon->ID);?>
						<div class="col text-center <?php if($count==1) {?>active<?php }?>"><img class="svg <?php echo $clientIcon->post_name;?>" src="<?php echo $icon['url'];?>"></div>
					<?php endforeach; ?>
				</div>
			</div>
			
		</div>
		
	</div>
	
	<div class="container-fluid">
		
		<div class="row testimonials">
			
			<span class="left"></span>
			<span class="right"></span>
			<div class="testimonial-wrapper">
				<div class="testimonial-holder d-flex">
					<?php
					$count = 0;
					
					foreach ( $clientArray as $client ) : $count++;?>
					<div class="testimonial testimonial-<?php echo $count;?> justify-content-center align-items-center d-flex" style="background-image:url(<?php echo get_the_post_thumbnail_url($client->ID);?>)">
						<div class="col-md-6">
							<p class="lead text-center"><?php echo $client->post_content;?></p>
							<?php $icon = get_field('client-icon', $client->ID);?>
							<p class="text-center"><img src="<?php echo $icon['url'];?>"></p>
						</div>
						
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>

	</div>
	
	<div class="container">
		
		<div class="row pt-3 pb-3">
			
			<div class="col d-flex align-items-center">
				
				<h4 class="mb-2 mb-md-0">Our Services</h4>
				
			</div>
			
		</div>
		
		<div class="row">
			
			<?php $blockCount=0;
			
			if( have_rows('services') ): while ( have_rows('services') ) : the_row(); 
				
				$blockCount++;
				
				$blockBg = get_sub_field('background');
				$blockLogo = get_sub_field('logo');
			?>
			
			<div class="col-md-6 col-12 tile <?php the_sub_field('block_title');?> d-flex <?php if($blockCount == 1 || $blockCount == 3){?> pr-md-1 <?php } ?> pb-1">
				
				<div class="row align-items-end <?php if($blockCount == 1 || $blockCount == 3){?>  justify-content-start <?php }else{ ?> justify-content-end <?php } if($blockCount == 2 ){?> justify-content-md-start <?php } if($blockCount == 3 ){?> justify-content-md-end <?php } ?>" style="background-image:url('<?php echo $blockBg['url'];?>')">
					
					<div class="col-md-6 col-10 content">
						<img class="pb-2" src="<?php echo $blockLogo['url'];?>">
						<p><?php the_sub_field('text');?></p>
						<a href="<?php the_sub_field('link');?>">Learn about <?php the_sub_field('block_title');?> &rsaquo;</a>
					</div>
					
				</div>
				
			</div>
			
		<?php endwhile; endif; ?>
			
		</div>
		
	</div>
	
	<div class="container" id="carrier-partners">
	
		<div class="row align-items-center">
			
			<div class="col-12 col-md-6 text-center">
				<?php $carrierPartnerImage = get_field('carrier_partner_image'); 
					
					if($carrierPartnerImage) {?>
				
					<img src="<?php echo $carrierPartnerImage['url'];?>">
					<?php } ?>
			</div>
			
			<div class="col-12 col-md-5 text-center text-md-left">
				
				<h4 class="mb-2 mb-md-0"><?php the_field('carrier_partner_title');?></h4>
				
				<p><?php the_field('subtitle');?></p>
				
				<div class="carrier-logos d-flex align-items-end justify-content-between">
					<div class="row w-100">
					<?php $logoCount=0; 
				
					if( have_rows('logos') ): while ( have_rows('logos') ) : the_row(); ?>
					
					<?php if($logoCount == 4){?>
					</div>
					</div>
					<div class="d-none logo-holder" style="opacity:0">
						<div class="carrier-logos  d-flex align-items-end justify-content-between">
							<div class="row w-100">
							
					<?php }	
						
					$carrierPartnerLogo = get_sub_field('partner_logo');
					
					if($carrierPartnerLogo){?>
					
						<div class="col-3  mb-4 text-center"><img src="<?php echo $carrierPartnerLogo['url'];?>"></div>
						
					<?php }
					$logoCount++;
					endwhile; endif;?>
					
							</div>
							
						</div>
						
					</div>
				
				<a href="#" class="show-more" data-target=".logo-holder">+ Show more</a>
			</div>
			
		</div>
	
	</div>
	
	<div class="wrapper pt-5 pb-5" id="partners">
			
		<div class="container">
		
			<div class="row align-items-center services">
				
				<div class="col-12 col-md-5 text-center text-md-left">
					
					<h4 class="mb-2 mb-md-0"><?php the_field('tech_partner_title');?></h4>
					
					<p><?php the_field('tech_partner_subtitle');?></p>
					
					<div class="carrier-logos d-flex align-items-end justify-content-between">
						
						<div class="row w-100">
						
						<?php $logoCount=0; 
					
						if( have_rows('tech_partner_logos') ): while ( have_rows('tech_partner_logos') ) : the_row(); ?>
						
						<?php if($logoCount == 4){?>
						
						</div>
						</div>
						
						<div class="d-none logo-holder" style="opacity:0">
							<div class="carrier-logos  d-flex align-items-end justify-content-between">
								<div class="row w-100">
								
						<?php }	
							
						$techPartnerLogo = get_sub_field('partner_logos');
						
						if($techPartnerLogo){?>
						
							<div class="col-3  mb-4 text-center"><img src="<?php echo $techPartnerLogo['url'];?>"></div>
							
						<?php }
						$logoCount++;
						endwhile; endif;?>
						
								</div>
								
							</div>
							
						</div>
					
					<a href="#" class="show-more" data-target=".logo-holder">+ Show more</a>
				</div>
				
				<div class="col-12 col-md-7 text-center mb-4 mt-5">
					<?php $techPartnerImage = get_field('tech_partner_image'); 
						
						if($techPartnerImage) {?>
					
						<img src="<?php echo $techPartnerImage['url'];?>">
						<?php } ?>
				</div>
				
			</div>
			<?php get_template_part('content', 'contact'); ?>
		
		</div>
		
	</div>
	

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
