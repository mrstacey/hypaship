<div class="row mb-5 contact">
				
	<div class="col-12 col-md-6 m-auto text-center">
		
		<h4>Want to get in touch?</h4>
		<p>We want to hear from you! Please use our contact form or get in touch by phone or email</p>
		<a href="/contact" class="btn btn-primary">Contact Us</a>
		
	</div>
	
</div>

<div class="row justify-content-center">
	
	<div class="col-10 col-offset-1">
		<div class="row">
			<div class="col-6 col-md-3 mb-3">
				<img class="pb-2" src="/wp-content/uploads/2019/01/postal-colored.svg">
			</div>
			<div class="col-6 col-md-3 mb-3">
				<img class="pb-2" src="/wp-content/uploads/2019/01/carrier-colorer.svg">
			</div>
			<div class="col-6 col-md-3 mb-3">
				<img class="pb-2" src="/wp-content/uploads/2019/01/3pl-colored.svg">
			</div>
			<div class="col-6 col-md-3 mb-3">
				<img class="pb-2" src="/wp-content/uploads/2019/01/eCommerce-colored.svg">
			</div>
		</div>
	</div>
	
</div>