<?php
/**
 * The template for product.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
$bannerBg = get_field('banner_image');
$bannerLogo = get_field('banner_logo');
$bannerContent = get_field('banner_content');
$bannerScreenshot = get_field('banner_screenshot');
$primaryColor = get_field('primary_color');

if($primaryColor){?>
<style>
	.hero-content a.btn {color:<?php echo $primaryColor;?>}
	#page-wrapper h4 {color:<?php echo $primaryColor;?>}
	#page-wrapper a {color:<?php echo $primaryColor;?>}
	
</style>
<?php }?>

<div class="wrapper <?php echo strtolower( get_the_title() );?>" id="page-wrapper">
	
	<div class="jumbotron">
		
		<div class="slide d-flex align-items-stretch" style="background: url(<?php echo $bannerBg['url'];?>) no-repeat center center; background-size: cover">
			
				<div class="h-100 w-100 position-absolute" style="background-color:<?php echo $primaryColor;?>; opacity:.7"></div>
				
				<div class="container hero-content pt-5">
					
					<div class="row align-items-center h-100 justify-content-center">
						
						<?php if($bannerScreenshot){?>
						
							<div class="col-md-4 col-12 order-12 order-md-1 text-right screenshot">
								<img src="<?php echo $bannerScreenshot['url'];?>">
							</div>
							<div class="col-md-4 col-12 order-1 order-md-12 pt-5 pb-5 text-center text-md-left">
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
								<?php if($bannerContent){
									echo '<p class="lead">'.$bannerContent.'</p>';
								};?>
								<a href="/contact" class="btn btn-light">Enquire</a>
							</div>
							
						<?php }else{ ?>
						
							<div class="col-6 text-center">
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
								<?php if($bannerContent){
									echo '<p>'.$bannerContent.'</p>';
								};?>
								<a href="/contact" class="btn btn-light">Enquire</a>
							</div>
							
						<?php }?>
					</div>
				</div>
				

			
		</div>
		
	</div>


	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
			
			<div class="col-12 mt-5 mb-5">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- .row -->

	</div><!-- #content -->
	
	<?php get_template_part('content', 'dynamic'); ?>

</div><!-- #page-wrapper -->

<div class="wrapper pb-5">
		
	<div class="container">
		
		<?php get_template_part('content', 'contact'); ?>
		
	</div>

</div>

<?php get_footer(); ?>
