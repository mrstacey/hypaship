<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper clients" id="page-wrapper">
	
	<div class="jumbotron">
		
		<div class="slide d-flex align-items-stretch" style="">
			
			<div class="h-100 w-100 position-absolute bg-color"></div>
			
			<div class="container hero-content pt-5">
				
				<div class="row align-items-center h-100 justify-content-center">
					
						<div class="col-md-6 col text-center">
							
							<h1>Our Clients</h1>
							<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			
						</div>
						
				</div>
			</div>
			
		</div>
		
	</div>
	
	
<?php 
$count =0;
while ( have_posts() ) : the_post(); ?>

	<div class="container-fluid">
		
		<div class="row testimonials">
					
			<div class="testimonial testimonial-<?php echo $count;?> justify-content-center align-items-center d-flex" style="background-image:url(<?php echo get_the_post_thumbnail_url($post->ID);?>)">
				
				<div class="col-md-6">
					<p class="lead text-center"><?php echo $post->post_content;?></p>
					<?php $icon = get_field('client-icon', $post->ID);?>
					<p class="text-center"><img src="<?php echo $icon['url'];?>"></p>
				</div>
				
			</div>
			
		</div>

	</div>
	
<?php $count++; endwhile; // end of the loop. ?>

</div><!-- #page-wrapper -->

<div class="wrapper pb-5">
		
	<div class="container">
		
		<?php get_template_part('content', 'contact'); ?>
		
	</div>

</div>

<?php get_footer(); ?>
