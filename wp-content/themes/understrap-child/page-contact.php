<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
$bannerBg = get_field('banner_image');
$bannerLogo = get_field('banner_logo');
$bannerContent = get_field('banner_content');
$bannerScreenshot = get_field('banner_screenshot');
$primaryColor = get_field('primary_color');

if($primaryColor){?>
<style>
	.hero-content a.btn {color:<?php echo $primaryColor;?>}
	#page-wrapper h4 {color:<?php echo $primaryColor;?>}
	#page-wrapper a {color:<?php echo $primaryColor;?>}
	
</style>
<?php }?>

<div class="wrapper <?php echo strtolower( get_the_title() );?>" id="page-wrapper">
	
	<div class="jumbotron">
		<?php if(isset($bannerBg) && $bannerBg){
			$style = "background: url(".$bannerBg['url'].") no-repeat center center; background-size: cover";
			$opacity = "opacity: 0.7";
		}else{
			$style ='';
			$opacity = "opacity: 1";
		}
		?>
		<div class="slide d-flex align-items-stretch" style="<?php echo $style; ?>">
			
			<div class="h-100 w-100 position-absolute" style="background-color:<?php echo $primaryColor.'; '.$opacity;?>;"></div>
			
			<div class="container hero-content pt-5">
				
				<div class="row align-items-center h-100 justify-content-center">
					
						<div class="col-md-6 col text-center">
							<?php if($bannerLogo){?>
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
							<?php }else{ 
								echo '<h1>';
								the_title();
								echo '</h1>';
							}
							if($bannerContent){
								echo '<p class="lead">'.$bannerContent.'</p>';
							};
							?>
			
						</div>
						
				</div>
			</div>
			
		</div>
		
	</div>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row mt-5 mb-5">
			
			<div class="col-12 col-md-3">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; // end of the loop. ?>
				
			</div>
			
			
			<div class="col-12 col-md-9">
				<?php echo do_shortcode('[contact-form-7 id="505" title="Contact form 1"]');?>
			
			</div>

		</div><!-- .row -->

	</div><!-- #content -->
	
	<section class="container-fluid pt-5 pb-5" id="locations_section">
		
		<div class="container">
			
			<div class="row align-items-center">
				
				<div class="col-md-4">
					
					<h1>Locations</h1>
					
				<?php if( have_rows('locations') ): while ( have_rows('locations') ) : the_row(); ?>
					
					<h5><?php the_sub_field('country');?></h5>
					
					<?php if( have_rows('sub-locations') ): ?>
					
						<ul>
							
						<?php while ( have_rows('sub-locations') ) : the_row(); ?>
						
							<li><?php the_sub_field('location_title');?></li>
							
						<?php endwhile;?>
						
						</ul>
					<?php endif; ?>
					
				<?php endwhile; endif;?>
					
				</div>
				
				<div class="col-md-8 h-100">
					
					<?php $mapIamge = get_field('map_image');?>
					<img class="align-middle" src="<?php echo $mapIamge['url'];?>">
					
				</div>
			</div>
			
		</div>
		
	</section>

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
