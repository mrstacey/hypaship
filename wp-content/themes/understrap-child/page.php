<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
$bannerBg = get_field('banner_image');
$bannerLogo = get_field('banner_logo');
$bannerContent = get_field('banner_content');
$bannerScreenshot = get_field('banner_screenshot');
$primaryColor = get_field('primary_color');

if($primaryColor){?>
<style>
	.hero-content a.btn {color:<?php echo $primaryColor;?>}
	#page-wrapper h4 {color:<?php echo $primaryColor;?>}
	#page-wrapper a {color:<?php echo $primaryColor;?>}
	
</style>
<?php }?>

<div class="wrapper <?php echo strtolower( get_the_title() );?>" id="page-wrapper">
	
	<div class="jumbotron">
		<?php if(isset($bannerBg) && $bannerBg){
			$style = "background: url(".$bannerBg['url'].") no-repeat center center; background-size: cover";
			$opacity = "opacity: 0.7";
		}else{
			$style ='';
			$opacity = "opacity: 1";
		}
		?>
		<div class="slide d-flex align-items-stretch" style="<?php echo $style; ?>">
			
			<div class="h-100 w-100 position-absolute" style="background-color:<?php echo $primaryColor.'; '.$opacity;?>;"></div>
			
			<div class="container hero-content pt-5">
				
				<div class="row align-items-center h-100 justify-content-center">
					
					<?php if(isset($bannerScreenshot) && $bannerScreenshot){?>
					
						<div class="col-6 text-right screenshot">
							<img src="<?php echo $bannerScreenshot['url'];?>">
						</div>
						<div class="col-4 pt-5 pb-4">
							<?php if($bannerLogo){?>
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
							<?php }else{ 
								echo '<h1>';
								the_title();
								echo '<h1>';
							}
							if($bannerContent){
								echo '<p class="lead">'.$bannerContent.'</p>';
							};
							if('products' == get_post_type() ) {?>
								<a href="#" class="btn btn-light">Enquire</a>
							<?php }?>
						</div>
						
					<?php }else{ ?>
					
						<div class="col-md-6 col text-center">
							<?php if($bannerLogo){?>
								<img class="pb-3" src="<?php echo $bannerLogo['url'];?>">
							<?php }else{ 
								echo '<h1>';
								the_title();
								echo '</h1>';
							}
							if($bannerContent){
								echo '<p class="lead">'.$bannerContent.'</p>';
							};
							
							if('products' == get_post_type() ) {?>
								<a href="#" class="btn btn-light">Enquire</a>
							<?php }?>
						</div>
						
					<?php }?>
				</div>
			</div>
			
		</div>
		
	</div>

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">
			
			<div class="col-12 mt-5 mb-5">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>
			</div>

		</div><!-- .row -->

	</div><!-- #content -->
	
	<?php get_template_part('content', 'dynamic'); ?>

</div><!-- #page-wrapper -->

<div class="wrapper pb-5">
		
	<div class="container">
		
		<?php get_template_part('content', 'contact'); ?>
		
	</div>

</div>

<?php get_footer(); ?>
