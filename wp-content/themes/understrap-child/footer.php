<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper position-relative" id="wrapper-footer">
	
	<div class="color-line"></div>
	
	<div class="<?php echo esc_attr( $container ); ?>">


		<footer class="site-footer" id="colophon">

			<div class="row">
				
				<div class="col-md-1 text-md-right text-left mb-3">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/Hypaship-Box.svg">
				</div>
				
				<div class="col-md-6 text-left mb-4 mb-md-0">
					<p class="mb-0 mb-md-2"><a href="tel:0845-155-6868">0845-155-6868</a>&nbsp;&nbsp;&nbsp;<a href="mailto:info@hypaship.com">info@hypaship.com</a></p>
					<p>HypaShip, Deer Park House, Network Point,<br>Witney, Oxfordshire OX29 0YN, UK</p>
					<p class="mb-0">Copyright &copy; Hypaship 2019</p>
					<div class="row fine-print">
						<div class="col text-left"><a href="#">Terms of use</a>&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a>&nbsp;&nbsp;&nbsp;<a href="#">GDPR Statement</a></div>
					</div>
				</div>
				
				<div class="col-md-5 align-self-end">
					
					<div class="row">
						
						<div class="col-6 col-md-4 mb-4 mb-md-0 footer-menu">
							<h6>Products</h6>
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'footer-1',
									'container_class' => 'footer-menu-1',
									'depth'           => 2,
								)
							); ?>
							
						</div>
						<div class="col-6 col-md-4 mb-4 mb-md-0 footer-menu">
							<h6>About</h6>
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'footer-2',
									'container_class' => 'footer-menu-2',
									'depth'           => 2,
								)
							); ?>
						</div>
						<div class="col-6 col-md-4 mb-4 mb-md-0 footer-menu">
							<h6>Contact</h6>
							<?php wp_nav_menu(
								array(
									'theme_location'  => 'footer-3',
									'container_class' => 'footer-menu-3',
									'depth'           => 2,
								)
							); ?>
						</div>
					
					</div>
					
				</div>
				
			</div>

		</footer><!-- #colophon -->


	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

