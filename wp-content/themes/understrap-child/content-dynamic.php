<?php
	if( have_rows('content_type') ):

	     // loop through the rows of data
	    while ( have_rows('content_type') ) : the_row();
	    
	    if( get_row_layout() == 'team' ):
			 
			?>
			
			<section class="container pt-5 pb-5" id="team_section">
				
				<div class="row mb-5">
					
					<div class="col">
						
						<h1><?php the_sub_field('section_title');?></h1>
						
					</div>
					
				</div>
				
					
				<div class="row">
					
					<?php if( have_rows('management') ): while ( have_rows('management') ) : the_row(); ?>
					
						<div class="col-6 col-md-3 pl-md-5 pr-md-5 text-center">
							<?php $headshot = get_sub_field('headshot');?>
							<img src="<?php echo $headshot['url'];?>" alt="<?php the_sub_field('name');?>" class="rounded-circle mb-3">
							<h5><?php the_sub_field('name');?></h5>
							<p><?php the_sub_field('title') ;?> </p>
						</div>
					
					<?php endwhile; endif; ?>
					
				</div>
				
			</section>


			
			<?php elseif( get_row_layout() == 'history' ):
			 
				$milestoneCount = count(get_sub_field('milestone'));
			?>
			
			<section class="container-fluid pt-5 pb-5" id="history_section">
				
				<div class="row mb-4 controllers">
					
					<div class="col-md-4 col-12 ml-md-auto text-md-right">
						
						<a href="#" class="back disabled move-history">&lsaquo;</a>
						<a href="#" class="forward move-history">&rsaquo;</a>
						
					</div>
				</div>
				
				<div class="row align-items-center">
					
					<div class="col-12 col-md-4 ">
						<div class="col-md-8 col-12 float-right">
							<h1><?php the_sub_field('section_title') ;?></h1>
							<p><?php the_sub_field('section_subtitle') ;?></p>
						</div>
						
					</div>
					
					
					<div class="history-wrapper col-12 col-md-8 overflow-hidden">
						
						<?php if( have_rows('milestone') ): ?>
						
						<div class="history-holder" style="width:<?php echo ($milestoneCount*300);?>px">
							
							<hr style="width:<?php echo (($milestoneCount-1)*300);?>px">
							
							<div class="row">
								
								<?php while ( have_rows('milestone') ) : the_row(); ?>
								
								<div class="col milestone pr-5 pl-5 pr-md-0 pl-md-0">
									
									<div class="icon-holder mx-auto mx-md-0">
										
										<?php $historyIcon = get_sub_field('icon');?>
										<img src="<?php echo $historyIcon['url'];?>" >
										
									</div>
									
									<p><?php the_sub_field('text') ;?></p>
									
								</div>
								
								<?php endwhile; ?>
							</div>
						
						</div>
						
						<?php endif; ?>
						
					</div>
					
				</div>
			
			</section>

				

	       <?php elseif( get_row_layout() == 'screenshot_section' ): ?>
			
			<section id="screenshot_section">
				
				<div class="screenshot-wrapper">
					
					<div class="screenshot-holder pb-5 pb-md-0">
						
						<div class="container">
							
							<div class="row shot justify-content-center align-items-center d-flex">
							
								<div class="col-md-6 col-12 order-12 order-1 text-center text-md-left">
									
									<h4><?php the_sub_field('title'); ?></h4>
									<?php the_sub_field('content'); ?>
									
								</div>
								
								<div class="col-md-6 col-12 order-1 order-md-12 desktop">
									
									<?php $image = get_sub_field('image'); ?>
									<img src="<?php echo $image['url'];?>">
									
								</div>
								
							</div>
							
						</div>
						
					</div>
				
				</div>
				
			</section>
			
			
	
	       <?php  elseif( get_row_layout() == 'info_blocks' ): ?>
		       
		    <section class="container" id="info_blocks">
				
				<div class="row">
					
				<?php if( have_rows('block') ): while ( have_rows('block') ) : the_row();
					
					$icon = get_sub_field('icon'); ?>
					
					<div class="col-md-3 col-12 block mb-5">
						
						<div class="row">
						
							<div class="col-md-12 col-4 text-center">
								
								<img src="<?php echo $icon['url'];?>">
								
							</div>
							
							<div class="col-md-12 col-8 text-md-center text-left">
								
								<h5><?php the_sub_field('title');?></h5>
								
								<p><?php the_sub_field('content');?></p>
							
								<?php if(get_sub_field('link')){?>
								
									<a href="<?php the_sub_field('link');?>">Read more</a>
									
								<?php }?>
								
							</div>
						
						</div>
						
					</div>

				<?php endwhile; endif; ?>
				</div>
				
			</section>
	
	        	
	        	
	        <?php elseif( get_row_layout() == 'left_image_with_text' ): ?>
	
        	<section id="image_with_text">
	        	
	        	<div class="container-fluid">
	        	
		        	<div class="row">
			        	
			        	<?php 
				        $background = get_sub_field('image');
				        if(get_sub_field('contenting_position') == 'left'){
					        $contentOrder = 'order-md-1 order-12';
					        $imageOrder = 'order-md-12 order-12';
					    }else{
						    $contentOrder = 'order-md-12 order-12';
					        $imageOrder = 'order-md-1 order-12';
					    }
				        $solidColor = get_sub_field('solid_color');
				        $gradientStart = get_sub_field('gradient_start');
				        $gradientEnd = get_sub_field('gradient_end');
				        $textBg = get_sub_field('content_background') == 'solid' ? 'background-color:'.$solidColor.';' : 'background: linear-gradient(45deg, '.$gradientStart.' 0%,'.$gradientEnd.' 100%)';
						if($gradientStart) {
							$rgb = HTMLToRGB($gradientStart);
							$hsl = RGBToHSL($rgb);
							$textColor = $hsl->lightness < 200 ? 'light' : 'dark';
						}else {
							$textColor = 'dark';
						}
				         
				        ?>
			        	
			        	<div class="col-md-4 <?php echo $imageOrder;?>" style="background:url(<?php echo $background['url'];?>) no-repeat center center; background-size: cover;">
				        	
				        	<div class="row h-100 align-items-center justify-content-center">
					        	
					        	<div class="col-md-8 col image pb-5 pt-5">
						        	
						        	<h2><?php the_sub_field('overlay_title');?></h2>
						        	<p><?php the_sub_field('overlay_content');?></p>
						        	
					        	</div>	
					        	
				        	</div>
				        	
			        	</div>	
			        	<div class="col-md-8 <?php echo $contentOrder;?>" style="<?php echo $textBg; ?>">
				        	<div class="text <?php echo $textColor;?>" >
				        		<?php the_sub_field('content');?>
				        	</div>
			        	</div>	
			        	
		        	</div>
		        	
	        	</div>
	        	
        	</section>
        	
        	<?php elseif( get_row_layout() == 'full_width_banner' ): 
	        	
	        	$bannerBg = get_sub_field('background');
	        	$vertical; $horizontal;
	        	
	        	if(get_sub_field('position') == 'bottomLeft' || get_sub_field('position') == 'bottomCenter' || get_sub_field('position') == 'bottomRight'){
		        	$vertical = 'align-items-end';
	        	}elseif(get_sub_field('position') == 'center'){
		        	$vertical = 'align-items-center';
	        	}
	        	
	        	if(get_sub_field('position') == 'center' || get_sub_field('position') == 'bottomCenter'){
		        	$horizontal = 'text-center';
	        	}elseif(get_sub_field('position') == 'centerLeft' || get_sub_field('position') == 'bottomLeft'){
		        	$horizontal = 'text-left';
	        	}elseif(get_sub_field('position') == 'centerRight' || get_sub_field('position') == 'bottomRight'){
		        	$horizontal = 'text-right';
	        	}
        	?>
			
        	<section id="full_width_banner" class="d-flex <?php echo $vertical;?>" style="background:url('<?php echo $bannerBg['url'];?>') no-repeat center center; background-size: cover;">
	        	
	        	<div class="row w-100 ">
		        	
		        	<div class="col">
			        	
			        	<h1 class="<?php echo $horizontal;?>"><?php the_sub_field('banner_text');?></h1>
				        	
		        	</div>	
		        	
	        	</div>
	        	
        	</section>
	
	        <?php endif;
	        
	
	    endwhile;
	
	else :
	
	
	endif;
	
	?>