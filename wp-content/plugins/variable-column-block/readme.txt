=== Gutenberg Variable Columns ===
Contributors: ramizmanked
Tags: gutenberg, columns, gutenberg blocks, variable columns
Requires at least: 4.9
Tested up to: 5.0.1
Stable tag: trunk
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Allows you to add variable width block to Gutenberg.

== Description ==

Gutenberg is on its way to completely change the editing experience by moving to a block-based approach to content. It will bring more power to content editing. 

This plugin will work as add-on to Gutenberg's 'Column' block. 

Using this plugin, you will be able to add variable width column block to Gutenberg editor. If you required to divide your content into two non-equal width then this plugin is for you.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. That's it! Now you're ready to use 'Variable Columns' block in Gutenberg.


== Frequently Asked Questions ==

= Where can I find 'Variable Columns' block in Gutenberg? =

Edit any post/page with Gutenberg editor. You just need to click on 'Add Block' section on top-left corner of Gutenberg editor and goto Layout Elements. You'll see 'Variable Columns' block in it.

= How can I resize column width? =

To resize column width, you just need to select 'Variable Columns' block and drag range control displayed under 'Column Width' label according to your needs from right sidebar.

== Screenshots ==

1. Click on 'Add Block' button from Gutenberg editor
2. Select 'Variable Columns' block.
3. Adjust column width according to your requirement.
4. Here is the output of 'Variable Columns' block.

== Changelog ==

= 1.0.2 =
* readme.txt correction.

= 1.0.1 =
* "wp-editor" added in wp_register_script. "RichText" block is moved to 'wp-editor' component, previously was 'wp-blocks' component.

== Upgrade Notice ==

= 1.0.1 =
* Plugin gets broken due to recent library changes applied to Gutenberg.