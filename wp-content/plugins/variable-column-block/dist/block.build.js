/******/ (function(modules) {
    // webpackBootstrap
    /******/ // The module cache
    /******/ var installedModules = {}; // The require function
    /******/
    /******/ /******/ function __webpack_require__(moduleId) {
        /******/
        /******/ // Check if module is in cache
        /******/ if (installedModules[moduleId]) {
            /******/ return installedModules[moduleId].exports;
            /******/
        } // Create a new module (and put it into the cache)
        /******/ /******/ var module = (installedModules[moduleId] = {
            /******/ i: moduleId,
            /******/ l: false,
            /******/ exports: {}
            /******/
        }); // Execute the module function
        /******/
        /******/ /******/ modules[moduleId].call(
            module.exports,
            module,
            module.exports,
            __webpack_require__
        ); // Flag the module as loaded
        /******/
        /******/ /******/ module.l = true; // Return the exports of the module
        /******/
        /******/ /******/ return module.exports;
        /******/
    } // expose the modules object (__webpack_modules__)
    /******/
    /******/
    /******/ /******/ __webpack_require__.m = modules; // expose the module cache
    /******/
    /******/ /******/ __webpack_require__.c = installedModules; // define getter function for harmony exports
    /******/
    /******/ /******/ __webpack_require__.d = function(exports, name, getter) {
        /******/ if (!__webpack_require__.o(exports, name)) {
            /******/ Object.defineProperty(exports, name, {
                /******/ configurable: false,
                /******/ enumerable: true,
                /******/ get: getter
                /******/
            });
            /******/
        }
        /******/
    }; // getDefaultExport function for compatibility with non-harmony modules
    /******/
    /******/ /******/ __webpack_require__.n = function(module) {
        /******/ var getter =
            module && module.__esModule
                ? /******/ function getDefault() {
                      return module["default"];
                  }
                : /******/ function getModuleExports() {
                      return module;
                  };
        /******/ __webpack_require__.d(getter, "a", getter);
        /******/ return getter;
        /******/
    }; // Object.prototype.hasOwnProperty.call
    /******/
    /******/ /******/ __webpack_require__.o = function(object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
    }; // __webpack_public_path__
    /******/
    /******/ /******/ __webpack_require__.p = ""; // Load entry module and return exports
    /******/
    /******/ /******/ return __webpack_require__((__webpack_require__.s = 0));
    /******/
})(
    /************************************************************************/
    /******/ [
        /* 0 */
        /***/ function(module, exports) {
            var registerBlockType = wp.blocks.registerBlockType;
            var _wp$editor = wp.editor,
                RichText = _wp$editor.RichText,
                InspectorControls = _wp$editor.InspectorControls;
            var RangeControl = wp.components.RangeControl;

            registerBlockType(
                "gutenberg-variable-column-block/variable-columns",
                {
                    title: "Variable Columns",
                    description:
                        "Allows you to add variable width block to Gutenberg.",
                    icon: "text",
                    category: "layout",
                    attributes: {
                        paragraphLeft: {
                            type: "string",
                            selector: "p"
                        },
                        paragraphRight: {
                            type: "string",
                            selector: "p"
                        },
                        columnWidth: {
                            type: "number",
                            default: "50"
                        }
                    },

                    edit: function edit(_ref) {
                        var attributes = _ref.attributes,
                            className = _ref.className,
                            setAttributes = _ref.setAttributes;
                        var paragraphLeft = attributes.paragraphLeft,
                            paragraphRight = attributes.paragraphRight,
                            columnWidth = attributes.columnWidth;

                        function handleColumnWidthChange(newColumnWidth) {
                            setAttributes({ columnWidth: newColumnWidth });
                        }
                        return [
                            wp.element.createElement(
                                InspectorControls,
                                null,
                                wp.element.createElement(
                                    "div",
                                    { className: "variable-column-inspector" },
                                    wp.element.createElement(RangeControl, {
                                        label: "Column Width",
                                        value: columnWidth,
                                        onChange: handleColumnWidthChange,
                                        min: 10,
                                        max: 90
                                    })
                                )
                            ),
                            wp.element.createElement(
                                "div",
                                { className: "main-wrapper-editor" },
                                wp.element.createElement(
                                    "div",
                                    {
                                        className: "block-left-editor",
                                        style: { width: columnWidth + "%" }
                                    },
                                    wp.element.createElement(RichText, {
                                        tagName: "p",
                                        className: className,
                                        value: paragraphLeft,
                                        onChange: function onChange(
                                            newParagraphLeft
                                        ) {
                                            setAttributes({
                                                paragraphLeft: newParagraphLeft
                                            });
                                        },
                                        placeholder:
                                            "Start typing left content..."
                                    })
                                ),
                                wp.element.createElement(
                                    "div",
                                    {
                                        className: "block-right-editor",
                                        style: {
                                            width: 100 - columnWidth + "%"
                                        }
                                    },
                                    wp.element.createElement(RichText, {
                                        tagName: "p",
                                        className: className,
                                        value: paragraphRight,
                                        onChange: function onChange(
                                            newParagraphRight
                                        ) {
                                            setAttributes({
                                                paragraphRight: newParagraphRight
                                            });
                                        },
                                        placeholder:
                                            "Start typing right content..."
                                    })
                                )
                            )
                        ];
                    },
                    save: function save(_ref2) {
                        var attributes = _ref2.attributes,
                            className = _ref2.className;
                        var paragraphLeft = attributes.paragraphLeft,
                            paragraphRight = attributes.paragraphRight,
                            columnWidth = attributes.columnWidth;

                        return wp.element.createElement(
                            "div",
                            { className: "main-wrapper" },
                            wp.element.createElement(
                                "div",
                                {
                                    className: "block-left",
                                    style: { width: columnWidth + "%" }
                                },
                                wp.element.createElement(RichText.Content, {
                                    tagName: "p",
                                    className: className,
                                    value: paragraphLeft
                                })
                            ),
                            wp.element.createElement(
                                "div",
                                {
                                    className: "block-right",
                                    style: { width: 100 - columnWidth + "%" }
                                },
                                wp.element.createElement(RichText.Content, {
                                    tagName: "p",
                                    className: className,
                                    value: paragraphRight
                                })
                            )
                        );
                    }
                }
            );

            /***/
        }
        /******/
    ]
);
