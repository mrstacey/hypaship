<?php
/*
 * Plugin Name: Variable Column Block
 * Description: Allows you to add variable width block to Gutenberg.
 * Author: Ramiz Manked
 * Version: 1.0.2
 */

/* Exit if accessed directly. */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Initialize block on 'init'
 */
function gutenberg_variable_column_init_callback() {

	/* Register block script */
	wp_register_script(
		'gutenberg-variable-column-block-script',
		plugins_url( './dist/block.build.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-i18n', 'wp-editor' )
	);

	/* Register style for block admin area */
	wp_register_style(
		'gutenberg-variable-column-block-editor-style',
		plugins_url( './dist/editor.css', __FILE__ ),
		array( )
	);

	/* Register style for block fron end */
	wp_register_style(
		'gutenberg-variable-column-block-frontend-style',
		plugins_url( './dist/style.css', __FILE__ ),
		array( )
	);

	/* Register block type with Gutenberg */
	register_block_type( 'gutenberg-variable-column-block/variable-columns', array(
		'editor_script' => 'gutenberg-variable-column-block-script',
		'editor_style' => 'gutenberg-variable-column-block-editor-style',
		'style' => 'gutenberg-variable-column-block-frontend-style',
	) );
}
add_action( 'init', 'gutenberg_variable_column_init_callback' );

function gutenberg_variable_column_admin_callback() {
	
	wp_register_style(
		'gutenberg-variable-column-block-editor-style',
		plugins_url( './dist/editor.css', __FILE__ ),
		array('wp-edit-blocks' )
	);

	/* Register style for block fron end */
	wp_register_style(
		'gutenberg-variable-column-block-frontend-style',
		plugins_url( './dist/style.css', __FILE__ ),
		array( 'wp-edit-blocks' )
	);
}

add_action( 'admin_enqueue_scripts', 'gutenberg_variable_column_admin_callback' );